from nolearn.dbn import DBN
from sklearn.cross_validation import cross_val_score
from sklearn.preprocessing import scale
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import zero_one_loss, precision_score
from sklearn.cross_validation import train_test_split
import sys

from sklearn.cross_validation import cross_val_score
from sklearn.datasets import load_iris
from sklearn.preprocessing import scale
from sklearn.grid_search import ParameterGrid
import numpy as np
import pickle

test_errs = []

from os import path
data_dir = './synthetic/'


def get_data(filename):
    filename = path.join(data_dir, filename)
    m = np.loadtxt(filename, dtype=float)
    mr = m.reshape((-1, 1025))
    print filename, mr.shape
    return mr


files= ['apple.txt', 'apricot.txt', 'chestnut.txt', 'cotonwood.txt', 'fieldmaple.txt', 'hornbeam.txt', 'norwaymaple.txt', 'oak.txt', 'pear.txt', 'redmaple.txt']
data_sets = [get_data(f) for f in files]


data = np.vstack(data_sets)
np.random.shuffle(data)
X = data[:, :-1]
y = data[:,-1]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=1000)
print "Size of training", X_train.shape, y_train.shape
print "Size of testing", X_test.shape, y_train.shape
print "Number of target labels", len(set(y_train))
np.savetxt('training.txt', X_train)
np.savetxt('training_labels.txt', y_train)
np.savetxt('test.txt', X_test)
np.savetxt('testing_labels.txt', y_test)
