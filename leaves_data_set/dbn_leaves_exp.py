from nolearn.dbn import DBN
from sklearn.cross_validation import cross_val_score
from sklearn.preprocessing import scale
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import zero_one_loss, precision_score
from sklearn.cross_validation import train_test_split
import sys


from sklearn.cross_validation import cross_val_score
from sklearn.datasets import load_iris
from sklearn.preprocessing import scale
from sklearn.grid_search import ParameterGrid
import numpy as np
import pickle

test_errs = []


from os import path
data_dir = './synthetic/'

def get_data():
    X_train = np.loadtxt('./synthetic/training.txt', dtype=float)
    y_train = np.loadtxt('./synthetic/training_labels.txt', dtype=float)
    X_test = np.loadtxt('./synthetic/test.txt', dtype=float)
    y_test = np.loadtxt('./synthetic/testing_labels.txt', dtype=float)
    return X_train,  X_test, y_train, y_test


X_train, X_test, y_train, y_test = get_data() 
print "Size of training", X_train.shape, y_train.shape
print "Size of testing", X_test.shape, y_train.shape
print "Number of target labels", len(set(y_train))

param_map = {'max_norm': [32], 'noises':[[0.01, 0.05, 0.0025, 0.00125]]}
#param_map = {'max_norm': [-1], 'noises':[[]]}
pl = list(ParameterGrid(param_map))


num_feats = X_train.shape[1]
layers = [num_feats, 1024, 1024, len(set(y_train))]

def test_err_callback(clf, epoch):
    global test_errs
    y_preds = clf.predict(X_test)
    result = zero_one_loss(y_test, y_preds)
    print "Test error at epoch {} is {}".format(epoch, result)
    test_errs.append(result)

clf = DBN(
    layers,
    learn_rates=0.05,
    l2_costs = 0.0,
    epochs = 150,
    learn_rate_decays=1.0,
    verbose=True,
    nesterov = False,
    fine_tune_callback = test_err_callback,
    )


output_prefix = sys.argv[1]
output_dir = sys.argv[2]
for i, pv in enumerate(pl):
    test_errs = []
    print "On configuration:", i, pv
    clf.set_params(max_norm=pv['max_norm'], noises=pv['noises'])
    clf.fit(X_train, y_train)
    y_preds = clf.predict(X_test)
    result = (pv, clf, X_train, y_train, X_test, y_test, y_preds, zero_one_loss(y_test, y_preds), test_errs)
    output_filename = path.join(output_dir, output_prefix +  str(i) + '.pkl')
    pickle.dump(result, open(output_filename, 'wb'))
